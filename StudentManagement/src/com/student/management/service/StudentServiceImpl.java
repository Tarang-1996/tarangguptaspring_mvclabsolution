package com.student.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.management.dao.StudentDAO;
import com.student.management.entity.Student;



/**
 * 
 * @author Tarang Gupta
 *
 */

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentDAO studentDAO;

	
	/**
	 * to list all the students details
	 * 
	 * @return - list of type student
	 */
	@Override
	public List<Student> findAll() {
		
		return studentDAO.findAll();
	}
	
	
	/**
	 * to find the student by id
	 * 
	 * @param
	 * id - id of the student
	 * 
	 * @return - Object of type student
	 */

	@Override
	public Student findById(int id) {
		
		return studentDAO.findById(id);
	}
	
	
	/**
	 * to save the student
	 * 
	 * @param
	 * Student - Student object
	 * 
	 * @return - void
	 */

	@Override
	public void saveStudent(Student student) {
		
		studentDAO.saveStudent(student);

	}
	
	/**
	 * to delete the student
	 * 
	 * @param
	 * id - id of the student
	 * 
	 * @return - void
	 */

	@Override
	public void deleteById(int id) {
		
		studentDAO.deleteById(id);
	}

}
