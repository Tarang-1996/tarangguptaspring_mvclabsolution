package com.student.management.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.student.management.entity.Student;

@Repository
public class StudentDAOImpl implements StudentDAO {

	private SessionFactory sessionFactory = null;
	private Session session = null;

	public StudentDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;

		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {

			session = sessionFactory.openSession();
		}
	}
	
	
	/**
	 * to list all the students details
	 * 
	 * @return - list of type student
	 */

	@Override
	public List<Student> findAll() {
		
		Transaction transaction = session.beginTransaction();
		List<Student> students = session.createQuery("from Student").list();
		transaction.commit();
		return students;
	}

	/**
	 * to find the student by id
	 * 
	 * @param
	 * id - id of the student
	 * 
	 * @return - Object of type student
	 */
	
	@Override
	public Student findById(int id) {
				
		Transaction transaction= session.beginTransaction();
		Student student=session.get(Student.class, id);
		transaction.commit();
		return student;
	}
	
	/**
	 * to save the student
	 * 
	 * @param
	 * Student - Student object
	 * 
	 * @return - void
	 */
	@Override
	public void saveStudent(Student student) {
		
		Transaction transaction= session.beginTransaction();
		session.save(student);
		transaction.commit();
	}
	
	/**
	 * to delete the student
	 * 
	 * @param
	 * id - id of the student
	 * 
	 * @return - void
	 */

	@Override
	public void deleteById(int id) {
		
		Transaction transaction= session.beginTransaction();
		Student student=session.get(Student.class, id);
		session.delete(student);
		transaction.commit();
	
	}

}
