package com.student.management.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.student.management.entity.Student;

/**
 * 
 * @author Tarang Gupta
 *
 */

@Component
public interface StudentDAO {

	public List<Student> findAll();

	public Student findById(int id);

	public void saveStudent(Student student);

	public void deleteById(int id);

}
