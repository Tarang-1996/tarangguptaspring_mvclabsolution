<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport"
			content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<title>Student Management</title>
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
			integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
			crossorigin="anonymous">
		<style>
			h3 {
				color: Azure;
				text-align: center;
				font-style: oblique
			}
			
			body {
				background-color: #dc3545;
				text-align: center;
			}
		</style>
</head>
<body>
	<div class="container">
		<h3>WELCOME TO STUDENT MANAGEMENT SYSTEM</h3>
		<br>
		<br> <a href="student/list"
			class="btn btn-primary btn-lg btn-block">Click To Manage Student</a>
	</div>
</body>
</html>